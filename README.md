# docker-yml

## 介绍

集成常用的 docker-yml配置文件

## 模块介绍

```
├─docekr-mongo
├─docker-ceph
├─docker-elk
│  ├─config
│  └─logstash
├─docker-fastdfs
├─docker-hyperledger
├─docker-java
├─docker-jemeter
├─docker-kafka
├─docker-monitor
│  ├─cadvisor
│  ├─grafana
│  ├─influxdb
│  ├─prometheus
│  └─telegraf
├─docker-mysql
├─docker-nginx
│  ├─conf
│  ├─conf.d
│  └─logs
├─docker-redis
├─docker-zabbix
└─docker-zookeeper
```

## 加入我们

   欢迎各位开发者提出建议、push代码,如有疑问联系，请使用如下联系方式。邮箱:guanxf@aliyun.com,昵称:糊涂,交流qq群:140586555。