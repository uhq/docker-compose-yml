## fastDFS 容器安装

###  下载镜像
```
docker pull morunchang/fastdfs

```

###  运行tracker 
```
docker run -d --name tracker --net=host morunchang/fastdfs sh tracker.sh 
```

 
###  运行storage 
```
docker run -d --name storage --net=host -e TRACKER_IP=192.168.125.191:22122 -e GROUP_NAME=storagegroup morunchang/fastdfs sh storage.sh

```


* 影射虚拟机数据到主机：
```
docker run -d --name storage --net=host -e TRACKER_IP=192.168.125.191:22122 -e GROUP_NAME=storagegroup -v /home/mrray/fastdfs/fastdfs_data:/data/fast_data morunchang/fastdfs sh storage.sh
```
 

### 优化
https://blog.csdn.net/u010820702/article/details/79630115

###  操作：
* 进入到docker容器：
```
docker exec -it 9ed8787953e4 /bin/bash
   tracker：
   vi /etc/fdfs/tracker.conf
   storage:
   vi /etc/fdfs/storage.conf

```
HDFS
https://github.com/mdouchement/docker-hdfs