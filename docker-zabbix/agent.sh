#!/bin/bash
#add user
groupadd zabbix
useradd -g zabbix zabbix

#agent compile
yum  install -y wget autoconf automake gcc subversion make pkg-config
cd ~
mkdir zabbix32
cd zabbix32
svn co svn://svn.zabbix.com/branches/3.2 .
./bootstrap.sh
./configure --enable-agent
make install

#zabbix_module_docker.so compile
cd ~/zabbix32
mkdir src/modules/zabbix_module_docker
cd src/modules/zabbix_module_docker
wget https://raw.githubusercontent.com/monitoringartist/Zabbix-Docker-Monitoring/master/src/modules/zabbix_module_docker/zabbix_module_docker.c
wget https://raw.githubusercontent.com/monitoringartist/Zabbix-Docker-Monitoring/master/src/modules/zabbix_module_docker/Makefile
make

mkdir -p /usr/local/lib/zabbix/agent/
cp zabbix_module_docker.so /usr/local/lib/zabbix/agent/